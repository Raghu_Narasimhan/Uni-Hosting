import React from "react";
import { IXmlToJsxProps } from "App/modules/xmlToJsxGen";

const Name: React.FC<IXmlToJsxProps> = ({ content }) => {
    return <h1>{content.textContent}</h1>;
};
export default Name;
