import React from "react";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import makeStyles from "@material-ui/core/styles/makeStyles";
import { createStyles, Theme } from "@material-ui/core";
import Utilities from "App/services/Utilities";
import { IXmlToJsxProps } from "App/modules/xmlToJsxGen";
import ShowMarkdownFromXml from "App/modules/instruments/components/ShowMarkdownFromXml";
import DetailComp from "App/modules/instruments/widgets/shared/DetailComp";
import { RulebookLink } from "App/components/RulebookLink";
import ContactVendorLink from "App/components/ContactVendorLink";

export const genericInstrumentStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        block: {
            display: "block",
            [theme.breakpoints.up("sm")]: {
                display: "flex",
                alignItems: "center",
            },
        },
        label: {
            fontWeight: "lighter",
        },
        content: {
            fontWeight: 400,
        },
    })
);

const Instrument: React.FC<IXmlToJsxProps> = ({ content }) => {
    const classes = genericInstrumentStyles();
    const detailClasses = { label: classes.label, content: classes.content };
    const descriptionEl = Utilities.getDirectChild(content, "Description");

    const modelEl = Utilities.getDirectChildWithPath(content, "Properties/Model");
    const vendorEl = Utilities.getDirectChildWithPath(content, "Properties/Vendor");

    return (
        <Box className={classes.root}>
            <Box className={classes.block}>
                <Box p={1} display="flex" flex={1} alignItems="center">
                    <RulebookLink path={"/rulebook#instrument-documentation"}>
                        <Typography variant="h2" className={classes.label} style={{ whiteSpace: "nowrap" }}>
                            {`${vendorEl && vendorEl.textContent} ${modelEl && modelEl.textContent}`}
                        </Typography>
                    </RulebookLink>
                </Box>
            </Box>
            <Box>
                {vendorEl && <DetailComp title={"Vendor"} el={vendorEl} classes={detailClasses} />}
                {modelEl && <DetailComp title={"Model"} el={modelEl} classes={detailClasses} />}
                {vendorEl && vendorEl.textContent && <Box paddingX={1}><ContactVendorLink vendorName={vendorEl.textContent} /></Box>}
            </Box>
            <Box>
                {descriptionEl && (
                    <Box p={1}>
                        <ShowMarkdownFromXml content={descriptionEl.textContent} />
                    </Box>
                )}
            </Box>
        </Box>
    );
};
export default Instrument;
