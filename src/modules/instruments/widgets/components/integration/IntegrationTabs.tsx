import React, { useEffect, useState } from "react";
import { xmlToJsxMap } from "App/modules/instruments/widgets/WidgetsMapper";
import XmlRenderer from "App/modules/xmlToJsxGen/XmlRenderer";
import Tabs from "@material-ui/core/Tabs/Tabs";
import Tab from "@material-ui/core/Tab/Tab";
import Box from "@material-ui/core/Box";
import { XmlToJsxContext } from "App/modules/xmlToJsxGen/XmlToJsxGenerator";
import { IIntegration } from "App/modules/instruments/models/models";
import Typography from "@material-ui/core/Typography";
import Collapse from "@material-ui/core/Collapse";
import CircularProgress from "@material-ui/core/CircularProgress";
import Divider from "@material-ui/core/Divider";

const IntegrationTabs: React.FC<{ integrations: IIntegration[] }> = ({ integrations }) => {
    // tabs
    const [tabIndex, setTabIndex] = React.useState(0);
    const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => setTabIndex(newValue);

    useEffect(() => {
        console.log(integrations);
    }, [integrations]);

    return (
        <XmlToJsxContext.Provider value={xmlToJsxMap}>
            <Typography variant="h5" style={{ fontWeight: 300, whiteSpace: "nowrap" }}>
                Integrations
            </Typography>

            <Tabs value={tabIndex} onChange={handleChange} aria-label="integration tabs example">
                {integrations.map((item, index) => (
                    <Tab
                        key={index}
                        label={item.name}
                        id={`integration-tab-${index}`}
                        aria-controls={`integration-tabpanel-${index}`}
                    />
                ))}
            </Tabs>
            <Divider />
            {integrations.map((item, index) => (
                <IntegrationView key={index} index={index} tabIndex={tabIndex} integration={item} />
            ))}
        </XmlToJsxContext.Provider>
    );
};

export default IntegrationTabs;

const IntegrationView: React.FC<{ integration: IIntegration; index: number; tabIndex: number }> = ({
    integration,
    index,
    tabIndex,
}) => {
    const [fetched, setFetched] = useState(false);
    return (
        <>
            <Box
                role="tabpanel"
                hidden={tabIndex !== index}
                id={`integration-tabpanel-${index}`}
                aria-labelledby={`integration-tab-${index}`}
            >
                <div style={{ textAlign: "center" }}>
                    <Collapse
                        in={!fetched}
                        style={{
                            transitionDelay: !fetched ? "800ms" : "0ms",
                        }}
                        unmountOnExit
                    >
                        <Typography variant="h6" style={{ fontWeight: "lighter" }}>
                            Loading {integration.name}
                        </Typography>
                        <CircularProgress />
                    </Collapse>
                </div>
                <XmlRenderer
                    onSuccess={() => setFetched(true)}
                    url={`${process.env.PUBLIC_URL || ""}/${integration.filePath}`}
                />
            </Box>
        </>
    );
};
