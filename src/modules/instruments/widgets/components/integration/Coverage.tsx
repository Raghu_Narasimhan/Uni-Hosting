import React from "react";
import { IXmlToJsxProps, XmlToJsxChildrenGenerator } from "App/modules/xmlToJsxGen";
import Typography from "@material-ui/core/Typography/Typography";

const Coverage: React.FC<IXmlToJsxProps> = ({ content }) => (
    <>
        <Typography variant="subtitle1" style={{ fontWeight: "lighter" }}>
            Coverage
        </Typography>
        {content && content.textContent && <Typography>{content.textContent}</Typography>}
        <XmlToJsxChildrenGenerator content={content} noText />
    </>
);

export default Coverage;
