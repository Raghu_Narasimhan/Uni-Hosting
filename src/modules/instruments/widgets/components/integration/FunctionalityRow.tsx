import React from "react";
import { XmlToJsxChildrenGenerator } from "App/modules/xmlToJsxGen";
import Typography from "@material-ui/core/Typography";
import { genericInstrumentStyles } from "App/modules/instruments/widgets/components/Instrument";
import Grid from "@material-ui/core/Grid/Grid";
import { useGridStyles } from "App/modules/instruments/widgets/components/Integration";
import Box from "@material-ui/core/Box";

export type FunctionalityInterface = {
    methods: Element[];
    availability: string;
    interfaces: Element[];
    functionalityEl: Element;
};

const FunctionalityRow: React.FunctionComponent<{ functionality: FunctionalityInterface }> = ({ functionality }) => {
    const classes = genericInstrumentStyles();
    const gridClasses = useGridStyles();
    return (
        <Grid container item xs={12} className={`${gridClasses.bodyRows} ${gridClasses.border}`}>
            <Grid item xs={12} md={3} lg={2}>
                <Box style={{ display: "grid", height: "100%" }}>
                    {functionality.methods.map((method, index) => (
                        <Typography
                            key={index}
                            variant="h6"
                            className={`${classes.label} ${gridClasses.method} ${gridClasses.border}`}
                        >
                            {method.textContent}
                        </Typography>
                    ))}
                </Box>
            </Grid>
            <Grid item xs={12} md={3} lg={2}>
                <Box className={gridClasses.gridItem} style={{ display: "flex", height: "100%" }}>
                    <Typography variant="h6" className={classes.label}>
                        {functionality.availability}
                    </Typography>
                </Box>
            </Grid>
            <Grid item xs={12} md={6} lg={8} className={gridClasses.gridItem}>
                <XmlToJsxChildrenGenerator content={functionality.functionalityEl} />
            </Grid>
        </Grid>
    );
};
export default FunctionalityRow;
