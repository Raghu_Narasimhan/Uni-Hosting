import { IXmlToJsxContext, PathMapType, Empty, EmptyWithChildren, WildCardType } from "App/modules/xmlToJsxGen";
import { defaultMaterialTheme } from "App/services/styles";
import { ReactMarkdownUtils } from "App/components/reactMarkdown";
import Integration from "./components/Integration";
import Instrument from "./components/Instrument";
import Implementation, { GenericInterfaceComp } from "./components/integration/Implementation";
import Coverage from "App/modules/instruments/widgets/components/integration/Coverage";
import MinimumRequirements from "App/modules/instruments/widgets/components/integration/MinimumRequirements";
import Utilities from "App/services/Utilities";
import Vendor from "./components/integration/Vendor";

const pathMap: PathMapType = new Map([
    ["/Instrument", Instrument],
    ["/Instrument/Properties/Vendor", Empty],
    ["/Instrument/Properties/Model", Empty],
    ["/Instrument/Description", Empty],
    ["/Integration", Integration],
    ["/Integration/Name", Empty],
    ["/Integration/SupportedInstrument", Empty],
    ["/Integration/Vendor", Vendor],
    ["/Integration/MinimumRequirement", MinimumRequirements],
    ["/Integration/Description", Empty],
    ["/Integration/Functionality", Empty],
    ["/Integration/Functionality/Type", Empty],
    ["/Integration/Functionality/Availability", Empty],
    ["/Integration/Functionality/Interface", EmptyWithChildren],
]);

const wildCard: WildCardType[] = [
    {
        path: "/Integration/Functionality/Interface/*/Implementation",
        component: Implementation,
    },
    {
        path: "/Integration/Functionality/Interface/*/Coverage",
        component: Coverage,
    },
    {
        path: "/Integration/Functionality/Interface/*",
        component: GenericInterfaceComp,
    },
];

export const xmlToJsxMap: IXmlToJsxContext = {
    mapUtilities: {
        pathMap,
        wildCard,
    },
    exactPath: true,
    defaultTheme: defaultMaterialTheme,
    defaultReactMarkdownRenderers: ReactMarkdownUtils.MDRenderObject,
    markdownCodePreprocessor: Utilities.removeWhiteSpaceFromXmlContent,
};
