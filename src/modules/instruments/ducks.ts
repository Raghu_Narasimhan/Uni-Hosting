import { IPayloadAction, Reducer } from "App/types";
import { IInstrument, IIntegration, IInstrumentIntegration, IVendor } from "./models/models";
import { IGlobalState } from "App/appstate/store";
import { ThunkAction, ThunkDispatch } from "redux-thunk";
import { IInstrumentsState, initialState, InstrumentsLabel } from "App/modules/instruments/state";
import manager from "App/services/orm/RepositoryManager";
import { repoInfo } from "App/modules/instruments/models/entities";

/**
 * Actions
 */
export enum ActionType {
    // selector
    SELECT = "INSTRUMENTS_MODULE/INSTRUMENT/SELECT",

    // instruments
    INSTRUMENT_ADD = "INSTRUMENTS_MODULE/INSTRUMENT/ADD",
    INSTRUMENT_UPDATE = "INSTRUMENTS_MODULE/INSTRUMENT/UPDATE",
    INSTRUMENT_REMOVE = "INSTRUMENTS_MODULE/INSTRUMENT/REMOVE",

    // integrations
    INTEGRATIONS_ADD = "INSTRUMENTS_MODULE/INTEGRATION/ADD",
    INTEGRATIONS_UPDATE = "INSTRUMENTS_MODULE/INTEGRATION/UPDATE",
    INTEGRATIONS_REMOVE = "INSTRUMENTS_MODULE/INTEGRATION/REMOVE",

    // vendors
    VENDORS_ADD = "INSTRUMENTS_MODULE/VENDOR/ADD",
    VENDORS_UPDATE = "INSTRUMENTS_MODULE/VENDOR/UPDATE",
    VENDORS_REMOVE = "INSTRUMENTS_MODULE/VENDOR/REMOVE",

    // instrument <-> integrations
    INSTRUMENT_INTEGRATION_ADD = "INSTRUMENT_MODULE/INSTRUMENTS_INTEGRATION/ADD",
    INSTRUMENT_INTEGRATION_UPDATE = "INSTRUMENT_MODULE/INSTRUMENTS_INTEGRATION/UPDATE",
    INSTRUMENT_INTEGRATION_REMOVE = "INSTRUMENT_MODULE/INSTRUMENTS_INTEGRATION/REMOVE",
}

// selector
interface ISelector extends IPayloadAction<ActionType.SELECT, string | undefined> {}

const selectInstrument = (instrumentName: string | undefined): ISelector => ({
    type: ActionType.SELECT,
    payload: instrumentName,
});

// instrument <-> integration
interface IInstrumentIntegrationAdd
    extends IPayloadAction<ActionType.INSTRUMENT_INTEGRATION_ADD, IInstrumentIntegration> {}
interface IInstrumentIntegrationUpdate
    extends IPayloadAction<ActionType.INSTRUMENT_INTEGRATION_UPDATE, IInstrumentIntegration> {}
interface IInstrumentIntegrationRemove extends IPayloadAction<ActionType.INSTRUMENT_INTEGRATION_REMOVE, string> {}
export type InstrumentIntegrationActions =
    | IInstrumentIntegrationAdd
    | IInstrumentIntegrationUpdate
    | IInstrumentIntegrationRemove;

const addInstrumentIntegration = (instrumentIntegration: IInstrumentIntegration): IInstrumentIntegrationAdd => ({
    type: ActionType.INSTRUMENT_INTEGRATION_ADD,
    payload: instrumentIntegration,
});

const updateInstrumentIntegration = (instrumentIntegration: IInstrumentIntegration): IInstrumentIntegrationUpdate => ({
    type: ActionType.INSTRUMENT_INTEGRATION_UPDATE,
    payload: instrumentIntegration,
});

const removeInstrumentIntegration = (instrumentIntegrationId: string): IInstrumentIntegrationRemove => ({
    type: ActionType.INSTRUMENT_INTEGRATION_REMOVE,
    payload: instrumentIntegrationId,
});

// instruments
interface IInstrumentAdd extends IPayloadAction<ActionType.INSTRUMENT_ADD, IInstrument> {}
interface IInstrumentUpdate extends IPayloadAction<ActionType.INSTRUMENT_UPDATE, IInstrument> {}
interface IInstrumentRemove extends IPayloadAction<ActionType.INSTRUMENT_REMOVE, string> {}
export type InstrumentActions = IInstrumentAdd | IInstrumentUpdate | IInstrumentRemove;

const addInstrument = (instrument: IInstrument): IInstrumentAdd => ({
    type: ActionType.INSTRUMENT_ADD,
    payload: instrument,
});

const updateInstrument = (instrument: IInstrument): IInstrumentUpdate => ({
    type: ActionType.INSTRUMENT_UPDATE,
    payload: instrument,
});

const removeInstrument = (instrumentId: string): IInstrumentRemove => ({
    type: ActionType.INSTRUMENT_REMOVE,
    payload: instrumentId,
});

// integrations
interface IIntegrationAdd extends IPayloadAction<ActionType.INTEGRATIONS_ADD, IIntegration> {}
interface IIntegrationUpdate extends IPayloadAction<ActionType.INTEGRATIONS_UPDATE, IIntegration> {}
interface IIntegrationRemove extends IPayloadAction<ActionType.INTEGRATIONS_REMOVE, string> {}
export type IntegrationActions = IIntegrationAdd | IIntegrationUpdate | IIntegrationRemove;

const addIntegration = (integration: IIntegration): IIntegrationAdd => ({
    type: ActionType.INTEGRATIONS_ADD,
    payload: integration,
});

const updateIntegration = (integration: IIntegration): IIntegrationUpdate => ({
    type: ActionType.INTEGRATIONS_UPDATE,
    payload: integration,
});

const removeIntegration = (integrationId: string): IIntegrationRemove => ({
    type: ActionType.INTEGRATIONS_REMOVE,
    payload: integrationId,
});

// vendors
interface IVendorAdd extends IPayloadAction<ActionType.VENDORS_ADD, IVendor> {}
interface IVendorUpdate extends IPayloadAction<ActionType.VENDORS_UPDATE, IVendor> {}
interface IVendorRemove extends IPayloadAction<ActionType.VENDORS_REMOVE, string> {}
export type VendorActions = IVendorAdd | IVendorUpdate | IIntegrationRemove;

const addVendor = (vendor: IVendor): IVendorAdd => {
    console.log(vendor);
    return {
        type: ActionType.VENDORS_ADD,
        payload: vendor,
    };
};

const updateVendor = (vendor: IVendor): IVendorUpdate => ({
    type: ActionType.VENDORS_UPDATE,
    payload: vendor,
});

const removeVendor = (vendorId: string): IVendorRemove => ({
    type: ActionType.VENDORS_REMOVE,
    payload: vendorId,
});

// All actions
export type InstrumentModuleActions = ISelector | InstrumentIntegrationActions | InstrumentActions | IntegrationActions;

/**
 * Thunks
 */
type InstrumentsThunk = ThunkAction<any, IGlobalState, null, InstrumentModuleActions>;
export type InstrumentsDispatch = ThunkDispatch<IGlobalState, null, InstrumentModuleActions>;

// individual thunk processes
type SimpleInstrumentThunk = () => InstrumentsThunk;

/**
 * Public actions and thunks
 */
export const actions = {
    selectInstrument,

    // instrument <-> integration
    addInstrumentIntegration,
    updateInstrumentIntegration,
    removeInstrumentIntegration,

    // instrument
    addInstrument,
    updateInstrument,
    removeInstrument,

    // integration
    addIntegration,
    updateIntegration,
    removeIntegration,

    // vendor
    addVendor,
    updateVendor,
    removeVendor,
};

/**
 * Reducer
 */
const reduceSelectedInstrumentName = (
    state: IInstrumentsState,
    action: InstrumentModuleActions
): IInstrumentsState["selectedInstrumentId"] => {
    switch (action.type) {
        case ActionType.SELECT:
            return action.payload;
        default:
            return state.selectedInstrumentId;
    }
};

type ReducerType = Reducer<IInstrumentsState, InstrumentModuleActions>;
export const InstrumentsReducer: ReducerType = (state = initialState, action): IInstrumentsState => ({
    ...state,
    selectedInstrumentId: reduceSelectedInstrumentName(state, action),
});

/**
 * Register add, update and delete actions
 * and also get necessary repos for selectors
 */
const instrumentRep = manager.getRepository(repoInfo.instrumentsName);
instrumentRep.setOperations(ActionType.INSTRUMENT_ADD, ActionType.INSTRUMENT_UPDATE, ActionType.INTEGRATIONS_REMOVE);

const integrationRep = manager.getRepository(repoInfo.integrationsName);
integrationRep.setOperations(
    ActionType.INTEGRATIONS_ADD,
    ActionType.INTEGRATIONS_UPDATE,
    ActionType.INTEGRATIONS_REMOVE
);

const vendorRep = manager.getRepository(repoInfo.vendorsName);
vendorRep.setOperations(ActionType.VENDORS_ADD, ActionType.VENDORS_UPDATE, ActionType.VENDORS_REMOVE);

const instrumentIntegrationRep = manager.getRepository(repoInfo.instrumentIntegrationName);
instrumentIntegrationRep.setOperations(
    ActionType.INSTRUMENT_INTEGRATION_ADD,
    ActionType.INSTRUMENT_INTEGRATION_UPDATE,
    ActionType.INSTRUMENT_INTEGRATION_REMOVE
);

/**
 * Selectors
 */

/**
 * @ImplNote: we are assuming instrument name is unique, as we are using it for the URLs
 */
// instruments
const getInstruments = () => instrumentRep.orm().getAll<IInstrument>();
const getInstrumentByName = (name: string): IInstrument | undefined =>
    instrumentRep
        .orm()
        .getAll<IInstrument>()
        .find((instrument) => instrument.fullName === name);

// selected instrument
const getSelectedInstrumentId = (state: IGlobalState): string | undefined =>
    state[InstrumentsLabel.STATE].selectedInstrumentId;

const getSelectedInstrument = (state: IGlobalState): IInstrument | undefined => {
    const foundInstrument = instrumentRep
        .orm(state)
        .getAll<IInstrument>()
        .filter((instrument) => instrument.fullName === state[InstrumentsLabel.STATE].selectedInstrumentId)[0];
    return foundInstrument ? foundInstrument : undefined;
};

// integrations
const getIntegrations = () => integrationRep.orm().getAll<IIntegration>();
const getIntegrationById = (id: string) => integrationRep.orm().getById<IIntegration>(id);

// vendors
const getVendorByName = (name: string): IVendor | undefined =>
    vendorRep
        .orm()
        .getAll<IVendor>()
        .find((vendor) => vendor.name === name);

// instruments <-> integrations
const getIntegrationsFromInstrumentId = (instrumentId: string) => {
    const pivotInstrumentIntegration = instrumentRep
        .ormChildren(instrumentId, instrumentIntegrationRep.entityName)
        .getAll<IInstrumentIntegration>();
    const integrations: IIntegration[] = [];
    pivotInstrumentIntegration.forEach((pivotItem) => {
        const integration = integrationRep.orm().getById<IIntegration>(pivotItem.integrationId);
        if (integration) {
            integrations.push(integration);
        }
    });
    return integrations;
};

export const selectors = {
    getInstruments,
    getInstrumentByName,
    getSelectedInstrumentId,
    getSelectedInstrument,
    getIntegrations,
    getIntegrationById,
    getIntegrationsFromInstrumentId,
    getVendorByName,
};
