import { BaseApi } from "App/services/apis/BaseApi";
import { HTTPMethod } from "App/services/apis/RestClient";
import { IIndex } from "../models/models";

class UilApi extends BaseApi {
    public getIndex = (): Promise<IIndex> => this.sendRequest(HTTPMethod.GET, "index.json");
    public getXmlFile = (url: string): Promise<string> => this.requestXmlFile(url);
}

export const uilApi = new UilApi();
