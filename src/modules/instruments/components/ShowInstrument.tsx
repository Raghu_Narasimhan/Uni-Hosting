import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { Redirect, withRouter, RouteComponentProps } from "react-router-dom";
import { IGlobalState } from "App/appstate/store";
import { selectors } from "../ducks";
import Utilities from "App/services/Utilities";
import { REDIRECT_TIMEOUT } from "App/constants";
import Collapse from "@material-ui/core/Collapse";
import CircularProgress from "@material-ui/core/CircularProgress";
import { IInstrument, IIntegration } from "App/modules/instruments/models/models";
import makeStyles from "@material-ui/core/styles/makeStyles";
import { Card, createStyles, Theme } from "@material-ui/core";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import CardContent from "@material-ui/core/CardContent";
import IntegrationTabs from "App/modules/instruments/widgets/components/integration/IntegrationTabs";
import { xmlToJsxMap } from "App/modules/instruments/widgets/WidgetsMapper";
import XmlRenderer from "App/modules/xmlToJsxGen/XmlRenderer";
import { XmlToJsxContext } from "App/modules/xmlToJsxGen/XmlToJsxGenerator";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: "flex",
            flexDirection: "column",
            "& > *": {
                margin: theme.spacing(1),
            },
        },
    })
);

const mapStateToProps = (state: IGlobalState, ownProps: IShowInstrument) => {
    let instrumentIndex: IInstrument | undefined;
    if (ownProps.match && ownProps.match.params && ownProps.match.params.instrumentName) {
        instrumentIndex = selectors.getInstrumentByName(ownProps.match.params.instrumentName);
    }
    return {
        selectedInstrument: selectors.getSelectedInstrument(state),
        getIntegrationsFromInstrumentId: selectors.getIntegrationsFromInstrumentId,
        instrumentIndex,
    };
};

const mapDispatchToProps = {};

interface IShowInstrument {
    match?: { params: { instrumentName: string } }; // route params
}

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

interface IShowInstrumentProps extends StateProps, DispatchProps, IShowInstrument {}

const ShowInstrument: React.FunctionComponent<IShowInstrumentProps & RouteComponentProps<{}>> = ({
    instrumentIndex,
    getIntegrationsFromInstrumentId,
    match,
}) => {
    const classes = useStyles();
    const [remainingTime, setRemainingTime] = useState(REDIRECT_TIMEOUT);
    const [redirect, setRedirect] = useState(false);
    const [instrument, setInstrument] = useState<IInstrument | null>(null);
    const [integrations, setIntegrations] = useState<IIntegration[]>([]);

    useEffect(() => {
        Utilities.processRedirection(
            instrument,
            instrumentIndex,
            remainingTime,
            match.params.instrumentName,
            setRemainingTime,
            () => console.error,
            setRedirect
        );
    }, [remainingTime, instrumentIndex, instrument, match.params.instrumentName, getIntegrationsFromInstrumentId]);

    useEffect(() => {
        if (instrumentIndex) {
            setTimeout(() => {
                setIntegrations(getIntegrationsFromInstrumentId(instrumentIndex.id));
            });
        }
    }, [instrumentIndex, getIntegrationsFromInstrumentId]);

    return (
        <Box style={{ marginBottom: 50 }}>
            {redirect && <Redirect to={"/"} />}
            <div style={{ textAlign: "center" }}>
                <Collapse
                    in={instrument === null}
                    style={{
                        transitionDelay: instrument === null ? "800ms" : "0ms",
                    }}
                    unmountOnExit
                >
                    <Typography variant="h4">Loading instrument and integrations</Typography>
                    <CircularProgress />
                </Collapse>
            </div>

            <div className={classes.root}>
                <Card>
                    <CardContent>
                        <XmlToJsxContext.Provider value={xmlToJsxMap}>
                            {instrumentIndex && (
                                <XmlRenderer
                                    onSuccess={() => setInstrument(instrumentIndex)}
                                    url={`${process.env.PUBLIC_URL || ""}/${instrumentIndex.filePath}`}
                                />
                            )}
                            {integrations && <IntegrationTabs integrations={integrations} />}
                        </XmlToJsxContext.Provider>
                    </CardContent>
                </Card>
            </div>
        </Box>
    );
};
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ShowInstrument));
