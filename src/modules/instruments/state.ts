import { IORMState } from "App/services/orm";

export enum InstrumentsLabel {
    STATE = "instruments",
}

export interface IInstrumentsState extends IORMState {
    readonly maps: {
        readonly instruments: {};
        readonly integrations: {};
        readonly vendors: {};
        readonly instrumentIntegration: {};
    };
    readonly index: {};
    readonly selectedInstrumentId?: string;
}

export const initialState: IInstrumentsState = {
    maps: {
        instruments: {},
        integrations: {},
        vendors: {},
        instrumentIntegration: {},
    },
    index: {},
    selectedInstrumentId: undefined,
};
