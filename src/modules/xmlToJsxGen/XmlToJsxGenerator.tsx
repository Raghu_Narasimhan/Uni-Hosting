import React, { createContext, useContext } from "react";
import { Box, MuiThemeProvider, Typography } from "@material-ui/core";
import { TypographyProps } from "@material-ui/core/Typography";
import ReactMarkdown from "react-markdown";
import { IXmlToJsxProps } from "./index";
import createMuiTheme from "@material-ui/core/styles/createMuiTheme";
import { IXmlToJsxContext, XmlToJsxCom } from "App/modules/xmlToJsxGen/index";
import { XmlToJsxUtilities } from "App/modules/xmlToJsxGen/XmlToJsxUtilities";

// Context
export const XmlToJsxContext = createContext<IXmlToJsxContext>({
    mapUtilities: {
        pathMap: new Map(),
        wildCard: [],
    },
    defaultTheme: createMuiTheme(),
    defaultReactMarkdownRenderers: {},
    markdownCodePreprocessor: (content: string) => content,
});

export const DefaultWidget: XmlToJsxCom = ({ content, style }) => {
    const xmlToJsxContext = useContext<IXmlToJsxContext>(XmlToJsxContext);
    if (content.nodeName === "#text") {
        return content.textContent ? (
            <Box style={style}>
                <ReactMarkdown
                    renderers={xmlToJsxContext.defaultReactMarkdownRenderers}
                    source={
                        xmlToJsxContext.markdownCodePreprocessor
                            ? xmlToJsxContext.markdownCodePreprocessor(content.textContent)
                            : content.textContent
                    }
                />
            </Box>
        ) : (
            <></>
        );
    } else {
        return (
            <Box paddingLeft={2} style={style}>
                {content.tagName && <strong>{XmlToJsxUtilities.capitalize(content.tagName)}:</strong>}
                <XmlToJsxChildrenGenerator style={style} content={content} />
            </Box>
        );
    }
};

export const XmlToJsxTitle: React.FunctionComponent<IXmlToJsxProps & TypographyProps> = ({ content, ...props }) => (
    <>{content.tagName && <Typography {...props}>{XmlToJsxUtilities.capitalize(content.tagName)}: </Typography>}</>
);

export const XmlToJsxChildrenGenerator: React.FunctionComponent<{
    content: Element;
    noText?: boolean;
    style?: React.CSSProperties;
}> = ({ content, noText = false, style = {} }) => (
    <>
        {!noText &&
            Array.from(content.childNodes).map((child, index) => (
                <XmlToJsxGenerator style={style} key={index} content={child as Element} />
            ))}
        {noText &&
            Array.from(content.children).map((child, index) => (
                <XmlToJsxGenerator style={style} key={index} content={child as Element} />
            ))}
    </>
);

const XmlToJsxGenerator: XmlToJsxCom = ({ content, style }) => {
    const xmlToJsxContext = useContext<IXmlToJsxContext>(XmlToJsxContext);
    let Widget = undefined;
    if (xmlToJsxContext.exactPath) {
        Widget = XmlToJsxUtilities.getComponentMapper(
            xmlToJsxContext.mapUtilities ? xmlToJsxContext.mapUtilities.pathMap : new Map(),
            xmlToJsxContext.mapUtilities ? xmlToJsxContext.mapUtilities.wildCard : [],
            XmlToJsxUtilities.getXmlPath(content),
            xmlToJsxContext.exactPath
        );
    } else {
        Widget = xmlToJsxContext.mapUtilities ? xmlToJsxContext.mapUtilities.pathMap.get(content.tagName) : null;
    }
    return (
        <MuiThemeProvider theme={xmlToJsxContext.defaultTheme}>
            {Widget ? <Widget style={style} content={content} /> : <DefaultWidget style={style} content={content} />}
        </MuiThemeProvider>
    );
};
export default XmlToJsxGenerator;
