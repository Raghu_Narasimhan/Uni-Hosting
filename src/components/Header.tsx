import React from "react";
import { AppBar, createStyles, Theme } from "@material-ui/core";
import Toolbar from "@material-ui/core/Toolbar";
import makeStyles from "@material-ui/core/styles/makeStyles";
import { Link } from "react-router-dom";
import Button from "@material-ui/core/Button";
import Box from "@material-ui/core/Box";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        menuButton: {
            marginRight: theme.spacing(2),
        },
        title: {
            display: "none",
            textAlign: "center",
            [theme.breakpoints.up("sm")]: {
                display: "block",
            },
        },
    })
);

const Header: React.FunctionComponent = ({ children }) => {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <AppBar position="static">
                <Toolbar>
                    <Button color="inherit" className={classes.title} component={Link} to={"/"}>
                        HOME
                    </Button>
                    <Button color="inherit" className={classes.title} component={Link} to={"/rulebook"}>
                        RULEBOOK
                    </Button>
                    <Box flexGrow={1} />
                    <Button color="inherit" className={classes.title} component={Link} to={"/list"}>
                        List
                    </Button>
                    {children}
                </Toolbar>
            </AppBar>
        </div>
    );
};
export default Header;
