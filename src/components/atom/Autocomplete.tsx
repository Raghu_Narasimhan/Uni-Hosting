import React, { useEffect, useState } from "react";
import InputBase from "@material-ui/core/InputBase";
import Popper from "@material-ui/core/Popper";
import Paper from "@material-ui/core/Paper";
import ListItemText from "@material-ui/core/ListItemText";
import { createStyles, ListItem, Theme } from "@material-ui/core";
import makeStyles from "@material-ui/core/styles/makeStyles";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        inputRoot: {
            color: "inherit",
        },
        inputInput: {
            padding: theme.spacing(1, 1, 1, 7),
            transition: theme.transitions.create("width"),
            width: "100%",
            [theme.breakpoints.up("sm")]: {
                width: 120,
                "&:focus": {
                    width: 200,
                },
            },
        },
    })
);

interface IAutoCompleteProps {
    selected?: string;
    list: string[];
    onSelection: (item: string) => void;
}
const AutoComplete: React.FunctionComponent<IAutoCompleteProps> = ({ selected, list, onSelection }) => {
    const classes = useStyles();
    const [menuIsOpen, setMenuIsOpen] = useState(false);
    const [foundItems, setFoundItems] = useState<string[]>([]);
    const [anchorEl, setAnchorEl] = React.useState<HTMLInputElement | null>(null);
    const [itemIndex, setItemIndex] = useState<number | null>(null);
    const [inputValue, setInputValue] = useState("");

    useEffect(() => {
        if (selected) {
            setItemIndex(list.findIndex((val) => val === selected));
            setInputValue(selected);
        }
    }, [selected, list]);

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        if (anchorEl === null) {
            setAnchorEl(event.currentTarget);
        }

        if (event.target.value === "") {
            setMenuIsOpen(false);
        } else {
            setMenuIsOpen(true);
            const newFoundItems: string[] = [];
            for (let i = 0; i < list.length; i++) {
                if (list[i] && list[i].toLowerCase().indexOf(event.target.value.toLowerCase()) > -1) {
                    newFoundItems.push(list[i]);
                }
            }
            setFoundItems(newFoundItems);
        }
        setInputValue(event.target.value);
    };

    const incrementIndex = () => {
        if (itemIndex !== null) {
            setItemIndex(itemIndex + 1);
        } else {
            setItemIndex(0);
        }
    };
    const decrementIndex = () => {
        if (itemIndex !== null) {
            setItemIndex(itemIndex - 1);
        } else {
            setItemIndex(foundItems.length - 1);
        }
    };

    const onSelectionHandle = (index: number) => {
        onSelection(foundItems[index]);
        setInputValue(foundItems[index]);
        setMenuIsOpen(false);
    };

    useEffect(() => {
        // if we get out of index due to changes
        if (itemIndex !== null) {
            const count = foundItems.length - 1;
            if (itemIndex > count) {
                setItemIndex(0);
            } else if (itemIndex < 0) {
                setItemIndex(count);
            }
        }
    }, [itemIndex, foundItems]);

    const handleKeyUp = (event: React.KeyboardEvent) => {
        if (event.key === "ArrowDown") {
            incrementIndex();
        } else if (event.key === "ArrowUp") {
            decrementIndex();
        } else if (event.key === "Enter") {
            if (itemIndex !== null) {
                onSelectionHandle(itemIndex);
            } else {
                onSelectionHandle(0);
            }
        } else if (event.key === "Escape") {
            setItemIndex(null);
            setMenuIsOpen(false);
        }
    };

    return (
        <>
            <InputBase
                placeholder="Search…"
                classes={{
                    root: classes.inputRoot,
                    input: classes.inputInput,
                }}
                value={inputValue}
                inputProps={{ "aria-label": "search" }}
                onChange={handleChange}
                onKeyUp={handleKeyUp}
                autoComplete={"on"}
            />
            <Popper
                placement="bottom-end"
                style={{ minWidth: "300px", maxWidth: "90%" }}
                open={menuIsOpen}
                anchorEl={anchorEl}
            >
                <Paper>
                    <ListItemText>
                        {foundItems.map((item, index) => (
                            <ListItem
                                style={itemIndex === index ? { backgroundColor: "antiquewhite" } : {}}
                                key={item}
                                button
                                onClick={() => onSelectionHandle(index)}
                            >
                                {item}
                            </ListItem>
                        ))}
                    </ListItemText>
                </Paper>
            </Popper>
        </>
    );
};

export default AutoComplete;
