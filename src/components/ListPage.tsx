import React, { ReactNode } from "react";
import { Typography } from "@material-ui/core";
import Box from "@material-ui/core/Box";
import { selectors } from "App/modules/instruments/ducks";
import { connect } from "react-redux";
import ListItemText from "@material-ui/core/ListItemText";
import { IInstrument, IIntegration } from "App/modules/instruments/models/models";
import { Link } from "react-router-dom";
import { SHOW_INSTRUMENT_ROUTE } from "App/App";
import Button from "@material-ui/core/Button";
import Tag from "App/modules/instruments/widgets/shared/Tag";

export const LIST_ROUTE = "/list";

const mapStateToProps = () => ({
    instruments: selectors.getInstruments(),
    getIntegrationsFromInstrumentId: selectors.getIntegrationsFromInstrumentId,
});

const mapDispatchToProps = {};

interface IListPage {}

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

interface IListPageProps extends StateProps, DispatchProps, IListPage {}

const ListPage: React.FunctionComponent<IListPageProps> = ({ instruments, getIntegrationsFromInstrumentId }) => {
    const sortInstAlphabetically = (a: IInstrument, b: IInstrument) => {
        if (a.fullName[0] > b.fullName[0]) {
            return 1;
        }
        if (a.fullName[0] < b.fullName[0]) {
            return -1;
        }
        return 0;
    };

    return (
        <Box p={1}>
            <Typography variant="h1">Instrument list</Typography>
            <ListItemText>
                {instruments.length === 0 && <Typography>No instruments were found</Typography>}
                {instruments.length === 1 && <Typography>One instrument was found</Typography>}
                {instruments.length > 1 && <Typography>There are {instruments.length} instruments.</Typography>}
                {instruments &&
                    instruments
                        .sort(sortInstAlphabetically)
                        .map((instrument) => (
                            <InstrumentItem
                                key={instrument.id}
                                instrument={instrument}
                                integrations={getIntegrationsFromInstrumentId(instrument.id)}
                            />
                        ))}
            </ListItemText>
        </Box>
    );
};
export default connect(mapStateToProps, mapDispatchToProps)(ListPage);

const InstrumentItem: React.FunctionComponent<{ instrument: IInstrument; integrations: IIntegration[] }> = ({
    instrument,
    integrations,
}) => {
    const createFromInterfaceTypes = (integrationId: string, list: Set<string>): ReactNode => {
        const interfaceTypes = Array.from(list.values());
        return (
            <>
                {interfaceTypes.map((type) => (
                    <Tag key={`${integrationId}_${type}`} label={type} />
                ))}
            </>
        );
    };

    return (
        <Box paddingTop={1} paddingBottom={1}>
            <Typography variant={"h5"}>
                <Button
                    color="inherit"
                    style={{ textTransform: "unset" }}
                    component={Link}
                    to={SHOW_INSTRUMENT_ROUTE + "/" + instrument.fullName}
                >
                    {instrument.model} by {instrument.vendor}
                    {integrations.map((integration) => (
                        <React.Fragment key={integration.id}>
                            {createFromInterfaceTypes(integration.id, integration.interfaceTypes)}
                        </React.Fragment>
                    ))}
                </Button>
            </Typography>
        </Box>
    );
};
