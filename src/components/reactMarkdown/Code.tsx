import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import SyntaxHighlighter from "react-syntax-highlighter";
// @ts-ignore
import { docco } from "react-syntax-highlighter/dist/cjs/styles/hljs";

class CodeBlock extends PureComponent<any, { language: string; value: string }> {
    static propTypes = {
        value: PropTypes.string.isRequired,
        language: PropTypes.string,
    };

    static defaultProps = {
        language: null,
    };

    render() {
        const { language, value } = this.props;
        return (
            <SyntaxHighlighter language={language} style={docco}>
                {value}
            </SyntaxHighlighter>
        );
    }
}

export default CodeBlock;
