import React from "react";
import List from "@material-ui/core/List/List";
import createStyles from "@material-ui/core/styles/createStyles";
import makeStyles from "@material-ui/styles/makeStyles/makeStyles";
import { Theme } from "@material-ui/core/styles/createMuiTheme";
import ListItem from "@material-ui/core/ListItem/ListItem";
import RadioButtonUncheckedIcon from "@material-ui/icons/RadioButtonUnchecked";

import Typography from "@material-ui/core/Typography/Typography";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        list: {
            flexGrow: 1,
            paddingTop: "0 !important",
            paddingBottom: "0 !important",

            display: "block",
            listStyleType: "disc",
            marginBlockStart: "1em",
            marginBlockEnd: "1em",
            marginInlineStart: "0px",
            marginInlineEnd: "0px",
            paddingInlineStart: "40px",
            lineHeight: 1.43,
        },
        item: {
            paddingTop: "0 !important",
            paddingBottom: "0 !important",
            display: "list-item !important",
            textAlign: "match-parent" as "match-parent",
        },
        itemText: {
            paddingTop: "2px !important",
            paddingBottom: "2px !important",
        },
    })
);

export const MDListItemRender: React.FC = (props) => {
    const classes = useStyles();
    return (
        <ListItem className={classes.item}>
            <RadioButtonUncheckedIcon style={{ fontSize: 8, marginRight: 5, bottom: "2px", position: "inherit" }} />
            <Typography variant="body1" component="span" className={classes.itemText}>
                {props.children}
            </Typography>
        </ListItem>
    );
};

export const MDListRender: React.FC = (props) => {
    const classes = useStyles();
    return <List className={classes.list}>{props.children}</List>;
};
