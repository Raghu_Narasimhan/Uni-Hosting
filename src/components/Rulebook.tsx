import React, { useEffect, useState } from "react";
import ReactMarkdown from "react-markdown";
import rulebook from "../websiteContent/rulebook.md";
import Table from "@material-ui/core/Table/Table";
import {TableHead, Typography} from "@material-ui/core";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import TableRow from "@material-ui/core/TableRow";
import makeStyles from "@material-ui/core/styles/makeStyles";
import { ReactMarkdownUtils } from "App/components/reactMarkdown";

const SCROLL_TIMEOUT = 1000;
const SCROLL_INTERVAL = 50;

const useStyles = makeStyles({
    table: {
        width: "unset",
    },
});

function flatten(text: any, child: any): any {
    return typeof child === 'string'
        ? text + child
        : React.Children.toArray(child.props.children).reduce(flatten, text)
}

const HeadingWithAnchorRenderer: React.FunctionComponent<any> = (props: any) => {
    const children = React.Children.toArray(props.children);
    const text = children.reduce(flatten, '');
    const slug = text.toLowerCase().replace(/\W/g, '-');
    return (
        <Typography variant={"h" + props.level} {...props} style={{paddingTop: 20}} id={slug} >
            {props.children}
        </Typography>
    );
};

const Rulebook: React.FunctionComponent = () => {
    const [renderingTime, setRenderingTime] = useState(0);

    useEffect(() => {
        const hash = window.location.hash;
        if (hash && renderingTime < SCROLL_TIMEOUT) {
            const elements = document.getElementsByName(hash.substr(1));
            if (elements.length > 0) {
                elements.forEach((element) => {
                    element.scrollIntoView();
                });
            } else {
                setTimeout(() => {
                    setRenderingTime(renderingTime + SCROLL_INTERVAL);
                }, SCROLL_INTERVAL);
            }
        }
    }, [renderingTime]);

    const classes = useStyles();

    return (
        <ReactMarkdown
            escapeHtml={false}
            source={rulebook}
            renderers={{
                paragraph: ReactMarkdownUtils.MDRenderersCustom.paragraph,
                heading: HeadingWithAnchorRenderer,
                link: ReactMarkdownUtils.MDRenderersCustom.link,
                table: (props) => <Table className={classes.table}>{props.children}</Table>,
                tableHead: (props) => <TableHead>{props.children}</TableHead>,
                tableBody: (props) => <TableBody>{props.children}</TableBody>,
                tableRow: (props) => <TableRow>{props.children}</TableRow>,
                tableCell: (props) => <TableCell>{props.children}</TableCell>
            }}
        />
    );
};

export default Rulebook;
