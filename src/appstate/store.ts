import { createStore, combineReducers, compose, applyMiddleware, AnyAction, Store } from "redux";
import thunk from "redux-thunk";
import { connectRouter, routerMiddleware, RouterState } from "connected-react-router";
import AppHistory from "App/services/appHistory";
import { Reducer } from "App/types";
import { AppLabel, IAppState } from "App/appstate/state";
import { AppReducer } from "App/appstate/ducks";
import InstrumentsModule from "App/modules/instruments/instrumentsModule";
import { IInstrumentsState, InstrumentsLabel } from "App/modules/instruments/state";
import { InstrumentsReducer } from "App/modules/instruments/ducks";
import { IDictionary, manager } from "App/services/orm";

declare global {
    // tslint:disable-next-line
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION__: any;
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any;
    }
}

const enhancers = [];
const middleware = [thunk, routerMiddleware(AppHistory)];

if (process.env.NODE_ENV === "development") {
    const devToolsExtension = window.__REDUX_DEVTOOLS_EXTENSION__;

    if (typeof devToolsExtension === "function") {
        enhancers.push(devToolsExtension());
    }
}

const composeEnhancers =
    typeof window === "object" && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
        ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
              // Specify extension’s options like
              // name actionsBlacklist, actionsCreators, serialize...
          })
        : compose;

const composedEnhancers = composeEnhancers(
    applyMiddleware(...middleware)
    // other store enhancers if any
);

/**
 * IGlobalState type definition
 */
export interface IGlobalState extends IDictionary {
    router: RouterState;
    /**
     * @implNote: adds to IGlobalState type def
     */
    [AppLabel.STATE]: IAppState;
    [InstrumentsLabel.STATE]: IInstrumentsState;
}

/**
 * Store definition
 */
const combinedReducers = combineReducers<IGlobalState>({
    router: connectRouter(AppHistory),
    /**
     * @implNote: further application reducers should be added here
     */
    [AppLabel.STATE]: AppReducer,
    [InstrumentsLabel.STATE]: InstrumentsReducer,
});

/**
 * rootReducer
 * combines combinedReducers with all our entities
 * remember app specific actions changes execute first, then entities change will trigger (this is why thunks exist)
 */
const rootReducer: Reducer<IGlobalState | undefined, AnyAction> = (
    state: IGlobalState | undefined,
    action: AnyAction
): IGlobalState | undefined => {
    const intermediateState = combinedReducers(state, action);
    return manager.getAllEntityReducers()(intermediateState, action);
};
export const store: Store = createStore(rootReducer, undefined, composedEnhancers);

/**
 * Registers state getter for all entities
 * This way they can rely on it to find their path through the global state and manipulate themselves
 */
manager.registerStore(store);

/**
 * Calls initialization functions of modules
 */
InstrumentsModule.initialize(store);
