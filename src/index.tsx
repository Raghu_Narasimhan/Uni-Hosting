import React from "react";
import ReactDOM from "react-dom";

// third party vendors
import "font-awesome/scss/font-awesome.scss";
// eslint-disable-next-line
import $ from "jquery";
// eslint-disable-next-line
import Popper from "popper.js";
import "bootstrap/dist/js/bootstrap.bundle.min";

import App from "App/App";

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
