import { AnyAction } from "redux";
import { IDictionary, IndexedChildren, IObjectWithId, IORMState, ORMUtils } from "App/services/orm";
import { ORMSelectors } from "./ORMSelectors";
import manager from "./RepositoryManager";
import { IGlobalState } from "App/appstate/store";

/**
 * Enum
 */
enum ReducerOperations {
    SAVE = "SAVE",
    UPDATE = "UPDATE",
    DELETE = "DELETE",
    SAVE_COLLECTION = "SAVE_COLLECTION",
    UPDATE_COLLECTION = "UPDATE_COLLECTION",
    DELETE_COLLECTION = "DELETE_COLLECTION",
}

export enum Cascade {
    YES = "YES", // default
    NO = "NO",
    BIDIRECTIONAL = "BIDIRECTIONAL",
}

/**
 * Types
 */
interface RelationshipType {
    relatedTo: string;
    fk: string;
    cascade?: Cascade;
}

export type RelatedEntitiesMap = Map<string, RelationshipType>;

export type EntityRepositoryType<
    S extends IORMState = IORMState,
    A extends AnyAction = AnyAction,
    T extends IObjectWithId = IObjectWithId
> = EntityRepository<A, T>;

type ORMReducer<A extends AnyAction, T extends IObjectWithId> = <S extends IORMState>(
    entityRep: EntityRepository<A, T>
) => (state: S, actions: A) => S;

interface IOrmParentGetter {
    getParentId: (parentEntity: string, id: string) => string | undefined;
}
interface IOrmSimpleGetter {
    getAll: <T extends IObjectWithId>() => T[];
    getById: <T extends IObjectWithId>(id: string) => T | undefined;
}
interface IOrmGetter extends IOrmSimpleGetter, IOrmParentGetter {}

/**
 * RepoBuilder
 *
 * Responsible to build EntityRepositories. Can be used to construct a repository with the following:
 *  * construct through newBuilder
 *  * withReducer - a custom reducer supplied for this entity
 *  * withChildrenRelations - specifies how children relations are set (their entity names, cascading and foreign keys)
 *  * withParentEntityRepo - specifies owners of this entity (powerful when combined with children relations)
 *  * build - creates the entity repository
 */
export class RepoBuilder<A extends AnyAction, T extends IObjectWithId> {
    private _parents: RelatedEntitiesMap = new Map();
    private _children: Set<string> = new Set();

    private readonly _entityName: string;
    private readonly _statePath: string[];
    private _primaryKey: string = "id";
    private _reducer: ORMReducer<A, T> | undefined;

    public static newBuilder<A extends AnyAction, T extends IObjectWithId>(
        entityName: string,
        statePath: string
    ): RepoBuilder<A, T> {
        return new RepoBuilder(entityName, statePath);
    }

    private constructor(entityName: string, statePath: string, primaryKey: string = "id") {
        this._entityName = entityName;
        this._primaryKey = primaryKey;
        this._statePath = statePath.split(".");
    }

    public withReducer(reducer: ORMReducer<A, T>) {
        this._reducer = reducer;
        return this;
    }

    public withParents(relations: RelationshipType[]) {
        relations.forEach((e) => this._parents.set(e.relatedTo, e));
        return this;
    }

    public withPrimaryKey(key: string) {
        this._primaryKey = key;
        return this;
    }

    public build(): EntityRepository<A, T> {
        return new EntityRepository<A, T>(this);
    }

    public get entityName() {
        return this._entityName;
    }

    public get primaryKey() {
        return this._primaryKey;
    }

    public get statePath() {
        return this._statePath;
    }

    public get children() {
        return this._children;
    }

    public get parents() {
        return this._parents;
    }

    public get reducer() {
        return this._reducer;
    }
}

/**
 * EntityRepository class
 *
 * With this class we can have repositories that are responsible for
 * 1. managing relationships (parents and children)
 * 2. supply custom or generated reducers based on relationships, managing index tables by themselves
 * 3. have a simple repo orm that can help us retrieving objects by attributes, as well as relationships if they exist
 *
 */
export class EntityRepository<A extends AnyAction, T extends IObjectWithId> {
    private readonly _entityName: string;
    private readonly _pk: string;
    private readonly _statePath: string[];

    // relations from this entity (to other entities)
    private readonly parents: RelatedEntitiesMap = new Map();
    private readonly children: Set<string> = new Set();

    private readonly reducer: ORMReducer<A, T> | undefined;
    private readonly map: Map<ReducerOperations, string> = new Map();

    constructor(entityRepBuilder: RepoBuilder<A, T>) {
        this._entityName = entityRepBuilder.entityName;
        this._pk = entityRepBuilder.primaryKey;
        this._statePath = entityRepBuilder.statePath;

        this.parents = entityRepBuilder.parents;
        this.children = entityRepBuilder.children;

        this.reducer = entityRepBuilder.reducer;
    }

    private static relTable(childName: string, relation: RelationshipType): string {
        return childName + "_by_" + relation.fk;
    }

    /**
     * Responsible for retrieving localState fro store or give globalState (optional argument)
     * Remember that store.getState() cannot be used when in reducer context, as it will break the app
     */
    private getLocalState(globalState: IGlobalState | null = null): IORMState {
        let newState: any = globalState ? globalState : manager.getStateFromStore();
        this._statePath.forEach((path) => {
            if (newState[path]) {
                newState = newState[path];
            } else {
                throw Error("State path for entity " + this.entityName + " is wrongly defined.");
            }
        });
        return newState;
    }

    /**
     * Recursive construction of GlobalState from LocalState
     */
    private addStateOneDeep<P extends IDictionary, C>(parentState: P, localState: C, i: number): P {
        let resultState: P;
        if (this._statePath.length === i + 1) {
            resultState = {
                ...parentState,
                [this._statePath[i]]: localState,
            };
        } else {
            if (parentState[this._statePath[i]]) {
                const newParent = parentState[this._statePath[i]];
                resultState = {
                    ...parentState,
                    [this._statePath[i]]: this.addStateOneDeep(newParent, localState, ++i),
                };
            } else {
                throw new Error(
                    "State path for entity " +
                        this.entityName +
                        " was not found during iteration of the following " +
                        this._statePath[i] +
                        " path"
                );
            }
        }
        return resultState;
    }

    private mergeLocalStateInGlobal(localState: IORMState, globalState: IGlobalState): IGlobalState {
        return this.addStateOneDeep(globalState, localState, 0);
    }

    // create reducer and
    private constructReducer(state: IGlobalState, action: A): IGlobalState {
        switch (action.type) {
            // we currently join SAVE and UPDATE
            case this.map.get(ReducerOperations.SAVE):
            case this.map.get(ReducerOperations.UPDATE):
                return this.saveOrUpdate(state, action.payload);
            case this.map.get(ReducerOperations.DELETE):
                return this.delete(state, action.payload);
            case this.map.get(ReducerOperations.SAVE_COLLECTION):
            case this.map.get(ReducerOperations.UPDATE_COLLECTION):
                return this.saveOrUpdateCollection(state, action.payload);
            case this.map.get(ReducerOperations.DELETE_COLLECTION):
                return this.deleteCollection(state, action.payload);
            default:
                return state;
        }
    }

    /**
     * CUD methods
     */
    private deleteCollection(globalState: IGlobalState, deleteIds: string[]): IGlobalState {
        let newGlobalState = globalState;
        deleteIds.forEach((id) => (newGlobalState = this.delete(globalState, id)));
        return newGlobalState;
    }

    /**
     * saveOrUpdate()
     * saves or updates state, adding current object to map with it's specific primaryKey
     */
    private saveOrUpdate(globalState: IGlobalState, newObject: IObjectWithId): IGlobalState {
        let newLocalState: IORMState = this.getLocalState(globalState);
        const oldObject: IObjectWithId | undefined = this.orm(globalState).getById(newObject.id);
        // add this entity to map
        newLocalState = ORMUtils.addToMapByPrimaryKey(newLocalState, this.entityName, this.pk, newObject);
        // we assign relations from parent entities,
        let newGlobalState = this.mergeLocalStateInGlobal(newLocalState, globalState);
        return this.createRelationshipFromOwners(newGlobalState, newObject, oldObject);
    }

    private saveOrUpdateCollection(globalState: IGlobalState, newObjects: IObjectWithId[]): IGlobalState {
        let newGlobalState = globalState;
        newObjects.forEach((newObject) => (newGlobalState = this.saveOrUpdate(newGlobalState, newObject)));
        return newGlobalState;
    }

    /**
     * createRelationshipFromOwners
     * creates all the necessary index tables based on this entity owners
     *
     * GlobalState is only passed to satisfy ::orm(globalState) calls
     */
    private createRelationshipFromOwners(
        globalState: IGlobalState,
        newObject: IDictionary,
        oldObject: IDictionary | undefined
    ): IGlobalState {
        let newLocalState = this.getLocalState(globalState);
        const objectId = newObject[this.pk];

        // hooks current entity/object to it's owners
        this.parents.forEach((relation) => {
            const relatedTable = EntityRepository.relTable(this.entityName, relation);
            if (oldObject) {
                // update indexes: we only need to be worried about this object parents, as any children should
                // worry only about it's own parents (only this currentObject[FK] was changed)
                newLocalState = ORMUtils.removeValueFromIndex(
                    newLocalState,
                    relatedTable,
                    oldObject[relation.fk],
                    objectId
                );
            }
            newLocalState = ORMUtils.addToIndex(newLocalState, relatedTable, newObject[relation.fk], objectId);
        });

        return this.mergeLocalStateInGlobal(newLocalState, globalState);
    }

    /**
     * delete()
     * Deletes entity based on supplied id. If entity relation cascades, we delete them
     * throughout index and map.
     *
     * @implNote: in this delete method we are mostly dealing with our own local state, only if the relationship
     * cascades, do we need to merge current newLocalState with newGlobalState
     */
    private delete<S extends IORMState>(globalState: IGlobalState, id: string): IGlobalState {
        const currentObject: IObjectWithId | undefined = this.orm(globalState).getById(id);
        if (currentObject === undefined) {
            if (manager.isInDebugMode()) {
                console.warn("No object from " + this.entityName + " with ID " + id + " was found!");
            }
            return globalState;
        }
        // for readability purposes
        let newGlobalState = globalState;
        // changes to the globalState do not impact localState, as such we only keep track of localState
        let newLocalState = this.getLocalState(newGlobalState);

        this.children.forEach((childName) => {
            const childRepo = manager.getRepository(childName);
            const relation = childRepo.parents.get(this.entityName);

            if (relation) {
                // get all children of this childName entity from this ID
                const children: IObjectWithId[] = this.ormChildren(id, childName, globalState).getAll();

                if (relation.cascade !== Cascade.NO) {
                    children.forEach((child) => {
                        // childRepo can be anywhere
                        newGlobalState = childRepo.delete(newGlobalState, child.id);
                        // now we need to re-assign local state
                        newLocalState = this.getLocalState(newGlobalState);
                    });
                }

                // remove key from children relationship (this object will be deleted from indexes)
                newLocalState = ORMUtils.removeKeyFromIndex(
                    newLocalState,
                    EntityRepository.relTable(childName, relation),
                    id
                );
                // globalState has to be updated for next iteration
                newGlobalState = this.mergeLocalStateInGlobal(newLocalState, newGlobalState);
            }
        });

        this.parents.forEach((relation) => {
            const parentRepo = manager.getRepository(relation.relatedTo);
            const parentId = (currentObject as IDictionary)[relation.fk];
            const parent = parentRepo.orm(newGlobalState).getById(parentId) as IDictionary;

            if (parent) {
                newLocalState = ORMUtils.removeValueFromIndex(
                    newLocalState,
                    EntityRepository.relTable(this.entityName, relation),
                    parent[parentRepo.pk],
                    id
                );
            }
            // globalState has to be updated for next iteration
            newGlobalState = this.mergeLocalStateInGlobal(newLocalState, newGlobalState);

            // cascade child to parent
            if (relation.cascade === Cascade.BIDIRECTIONAL) {
                if (Object.keys(newLocalState.maps).includes(parentRepo.entityName)) {
                    // remove parent from local state
                    newLocalState = ORMUtils.removeFromMap(newLocalState, parentRepo.entityName, parentId);
                } else {
                    // remove parent from global state
                    newGlobalState = parentRepo.delete(newGlobalState, parentId);
                }
            }
        });

        newLocalState = ORMUtils.removeFromMap(newLocalState, this.entityName, id);

        // return merged global state
        return this.mergeLocalStateInGlobal(newLocalState, newGlobalState);
    }

    /**
     * Unique key
     */
    public get pk() {
        return this._pk;
    }

    /**
     * Entity name (used to generate tables)
     */
    public get entityName() {
        return this._entityName;
    }

    /**
     * setOperations
     *
     * Sets up the map that caches the redux actions to use for CRUD operations
     * This is only useful if you do not use a custom reducer, but the generated one
     *
     * @param add - Redux action as a string used for ADD operations
     * @param update - Redux action as a string used for UPDATE operations
     * @param remove - Redux action as a string used for REMOVE operations
     * @param addCol - Redux action as a string used for SAVE COLLECTION
     * @param updateCol - Redux action as a string used for UPDATE COLLECTION
     * @param deleteCol - Redux action as a string used for DELETE COLLECTION
     */
    public setOperations(
        add: string,
        update: string,
        remove: string,
        addCol?: string,
        updateCol?: string,
        deleteCol?: string
    ) {
        this.map.set(ReducerOperations.SAVE, add);
        this.map.set(ReducerOperations.UPDATE, update);
        this.map.set(ReducerOperations.DELETE, remove);
        if (addCol) this.map.set(ReducerOperations.SAVE_COLLECTION, addCol);
        if (updateCol) this.map.set(ReducerOperations.UPDATE_COLLECTION, updateCol);
        if (deleteCol) this.map.set(ReducerOperations.DELETE_COLLECTION, deleteCol);
    }

    /**
     * reduce()
     * Responsible to return either a generated reducer or a custom created one.
     * Custom possibility is designed for complex manual table operations.
     */
    public reduce(state: IGlobalState, action: A): IGlobalState {
        if (this.reducer) {
            return this.mergeLocalStateInGlobal(this.reducer(this)(this.getLocalState(state), action), state);
        } else {
            return this.constructReducer(state, action);
        }
    }

    /**
     * Relationships
     * Designed as we might need to update relationships on later phases
     */
    // child relations
    public addChildRelation(entity: string) {
        this.children.add(entity);
    }

    public getChildRelation(entityName: string) {
        const childRepo = manager.getRepository(entityName);
        const childRelation = childRepo.parents.get(this.entityName);
        if (childRelation) {
            return childRelation;
        } else {
            throw Error(
                "No child entity relation found with name " + entityName + " for current entity " + this.entityName
            );
        }
    }

    public removeChildRelation(entityName: string) {
        this.children.delete(entityName);
    }

    // parent relationships
    public addParentRelation(relation: RelationshipType) {
        this.parents.set(relation.relatedTo, relation);
    }

    public getParentRelations(): RelatedEntitiesMap {
        return this.parents;
    }

    public removeParentRelation(entityName: string) {
        this.parents.delete(entityName);
    }

    /**
     * orm
     *
     * Is responsible for querying the state to help us easily retrieve specific objects
     * based on ID and child relationships.
     *
     * @param globalState is optional parameter, so selectors can use ::orm() (boilerplate optimization)
     *
     * Note: reducers cannot access state with store.getState(): check getLocalStateFromStore()
     */
    public orm(globalState: IGlobalState | null = null): IOrmGetter {
        const localState = this.getLocalState(globalState);

        const getAll = <T extends IObjectWithId>(): T[] => ORMSelectors.getAll(localState, this.entityName) as T[];

        const getById = <T extends IObjectWithId>(id: string): T | undefined =>
            ORMSelectors.getById(localState, this.entityName, id) as T | undefined;

        const getParentId = (parentEntity: string, id: string): string | undefined => {
            const currentObject: IDictionary | undefined = ORMSelectors.getById(localState, this.entityName, id);
            const repo = manager.getRepository(parentEntity).parents.get(this.entityName);
            if (repo && currentObject && currentObject[repo.fk]) {
                return currentObject[repo.fk];
            }
            return undefined;
        };

        return {
            getAll,
            getById,
            getParentId,
        };
    }

    /**
     * ormChildren
     *
     * Is responsible for querying the state to help us easily retrieve specific objects
     * based on ID and child relationships.
     *
     * @param parentId is the id of this entity object
     * @param entityName is the name of the child entity
     * @param globalState is optional parameter, so selectors can use ::ormChildren() (boilerplate optimization)
     *
     * Note: reducers cannot access state with store.getState(): check getLocalStateFromStore()
     */
    public ormChildren<O extends IObjectWithId>(
        parentId: string,
        entityName: string,
        globalState: IGlobalState | null = null
    ): IOrmSimpleGetter {
        const childRepo = manager.getRepository(entityName);
        const relationTable = EntityRepository.relTable(childRepo.entityName, this.getChildRelation(entityName));
        const relationIndex = childRepo.getLocalState(globalState).index[relationTable];

        let parentToChildrenIndex: IndexedChildren | undefined = undefined;
        if (relationIndex && relationIndex[parentId]) {
            parentToChildrenIndex = relationIndex[parentId];
        }
        return {
            getAll: <O extends IObjectWithId>(): O[] => {
                const children: O[] = [];
                if (parentToChildrenIndex) {
                    Object.keys(parentToChildrenIndex).forEach((child) => {
                        const childObject = childRepo.orm(globalState).getById<O>(child);
                        if (childObject) children.push(childObject);
                    });
                }
                return children;
            },
            getById: <O extends IObjectWithId>(childId: string) => {
                if (parentToChildrenIndex && parentToChildrenIndex.hasOwnProperty(childId)) {
                    return childRepo.orm(globalState).getById(childId);
                }
                return undefined;
            },
        };
    }
}
