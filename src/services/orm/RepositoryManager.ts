import { Store, Reducer, Action, AnyAction } from "redux";
import { EntityRepository, EntityRepositoryType } from "./EntityRepository";
import { IObjectWithId, IORMState } from ".";
import { IGlobalState } from "App/appstate/store";

/**
 * RepositoryManager
 *
 * Responsible for managing all the repositories, and connect with the store in a single stroke. This way there is
 * no need to edit store.ts every time you add a new entity repo.
 * This class should be singular throughout the whole application, but we leave the option open for you to have a
 * new manager for a new store, should your application have multi store. Just create a `new RepositoryManger()`
 */
class RepositoryManager {
    private repositories: Map<string, EntityRepositoryType<any, any>> = new Map();
    private store: Store | undefined;
    private debugMode: boolean = false;

    private getState(): IGlobalState {
        if (this.store) {
            return this.store.getState();
        } else {
            throw Error("You must register store to be able to get the state");
        }
    }

    private getEntitiesReducer<S extends IGlobalState>(
        repos: EntityRepositoryType[]
    ): Reducer<IGlobalState, AnyAction> {
        return (state, action): IGlobalState => {
            let newState: IGlobalState = { ...state } as IGlobalState;
            repos.forEach((repo) => (newState = { ...repo.reduce(newState, action) }));
            return newState;
        };
    }

    public setDebugMode(flag: boolean): void {
        this.debugMode = flag;
    }
    public isInDebugMode(): boolean {
        return this.debugMode;
    }

    public registerStore(store: Store): void {
        this.store = store;
    }

    public getStateFromStore(): IGlobalState {
        return this.getState();
    }

    public addRepository<S extends IORMState, A extends Action, T extends IObjectWithId>(
        newRepo: EntityRepository<A, T>
    ): void {
        // add existing child
        this.repositories.forEach((repo) => {
            const relations = repo.getParentRelations();
            relations.forEach((relation) => {
                if (relation.relatedTo === newRepo.entityName) {
                    newRepo.addChildRelation(repo.entityName);
                }
            });
        });
        // add newRepo as child of existing parent
        newRepo.getParentRelations().forEach((parentRelation) => {
            const parentRepo = this.repositories.get(parentRelation.relatedTo);
            if (parentRepo) {
                parentRepo.addChildRelation(newRepo.entityName);
            }
        });
        // add to repositories
        this.repositories.set(newRepo.entityName, newRepo as EntityRepository<Action, IObjectWithId>);
    }

    public getRepository(entityName: string): EntityRepositoryType {
        const repo = this.repositories.get(entityName);
        if (repo) {
            return repo;
        } else {
            throw Error("There is no repo registered for entity " + entityName);
        }
    }

    public getAllEntityReducers(): Reducer<IGlobalState, AnyAction> {
        return this.getEntitiesReducer(Array.from(this.repositories.values()));
    }
}

const manager = new RepositoryManager();
export default manager;
