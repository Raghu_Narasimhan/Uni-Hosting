import { EFFECT_TIME, REDIRECT_LOOP } from "App/constants";

const removeWhiteSpaceFromXmlContent = (content: string | null = "") => {
    if (content) {
        let newContent = "";
        let initialSpaceCounter: number | null = null;
        const contentLine = content.split("\n");

        contentLine.forEach((line) => {
            if (line === "") {
                newContent += "\n";
            }
            else {
                if (initialSpaceCounter === null) {
                    initialSpaceCounter = line.search(/\S/);
                    newContent += line.slice(initialSpaceCounter, line.length).concat("\n");
                } else {
                    newContent += line.slice(initialSpaceCounter, line.length).concat("\n");
                }
            }
        });
        return newContent;
    }
    return "";
};

const createUUID = () => {
    // http://www.ietf.org/rfc/rfc4122.txt
    const s = [];
    const hexDigits = "0123456789abcdef";
    for (let i = 0; i < 36; i++) {
        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = "4"; // bits 12-15 of the time_hi_and_version field to 0010
    s[19] = hexDigits.substr((s[19] && 0x3) || 0x8, 1); // bits 6-7 of the clock_seq_hi_and_reserved to 01
    s[8] = s[13] = s[18] = s[23] = "-";
    return s.join("");
};

type ProcessRedirectionType = <T>(
    foundEntity: T,
    propEntity: T,
    initialRemainingTime: number,
    urlKey: string,
    setRemainingTime: (time: number) => void,
    errorHandler: (message: string) => void,
    setRedirect: (newState: boolean) => void
) => void;

const processRedirection: ProcessRedirectionType = (
    foundEntity,
    propEntity,
    remainingTime,
    key,
    setRemainingTime,
    errorHandler,
    setRedirect
) => {
    if (foundEntity) {
        setRemainingTime(0);
        return;
    }

    if (remainingTime > 0) {
        if (propEntity) {
            setRemainingTime(0);
        } else {
            setTimeout(() => {
                setRemainingTime(remainingTime - REDIRECT_LOOP);
            }, REDIRECT_LOOP);
        }
    } else {
        if (!propEntity) {
            // end of line
            setTimeout(() => {
                errorHandler(`Process with key ${key} not found. You were redirected to available workflows.`);
                // the consequence of the redirect is to unmount this component
                setRedirect(true);
            }, EFFECT_TIME + REDIRECT_LOOP); // we need to wait for effect to finish before unmount
        }
    }
};

const getDirectChild = (content: Element, name: string): Element | null => {
    const children = getDirectChildren(content, name);
    return children.length === 0 ? null : children[0];
};

const getDirectChildWithPath = (content: Element, path: string): Element | null => {
    const children = getDirectChildrenWithPath(content, path);
    return children.length === 0 ? null : children[0];
};

const getDirectChildrenWithPath = (content: Element, path: string): Element[] => {
    const nameArray = path.split("/");
    let children: Element[] | null = null;
    nameArray.forEach((_path) => {
        if (children) {
            let newChildren: Element[] = [];
            children.forEach((el) => {
                newChildren = getDirectChildren(el, _path);
            });
            children = newChildren;
        } else {
            children = getDirectChildren(content, _path);
        }
    });
    return children || [];
};

const getDirectChildren = (content: Element, name: string): Element[] => {
    if (content && content.childNodes) {
        return Array.from(content.childNodes).filter((el) => el.nodeName === name) as Element[];
    } else {
        return [];
    }
};

const Utilities = {
    removeWhiteSpaceFromXmlContent,
    createUUID,
    processRedirection,
    getDirectChild,
    getDirectChildren,
    getDirectChildWithPath,
    getDirectChildrenWithPath,
};
export default Utilities;
