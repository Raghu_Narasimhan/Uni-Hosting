import querystring, { ParsedUrlQueryInput } from "querystring";

export enum HTTPMethod {
    POST = "post",
    GET = "get",
    PUT = "put",
    DELETE = "delete",
    PATCH = "patch",
    HEAD = "head",
}

export class RestRequestError {
    public readonly statusText: string;
    public readonly status: number;
    public readonly message: string;

    constructor(statusText: string, status: number, message: string) {
        this.statusText = statusText;
        this.status = status;
        this.message = message;
    }
}

export enum ContentType {
    JSON = "application/json",
    TEXT = "text/*",
    FORM_DATA = "multipart/form-data",
    ARRAY_BUFFER = "*/*",
    BLOB = "*/*",
    XML = "application/xml",
    AUTO = "",
}

export class RestRequestBuilder {
    private _method: HTTPMethod = HTTPMethod.POST;
    private _path: string = "/";
    private _query: string = "";
    private _body: any;
    private _contentType: ContentType = ContentType.JSON;
    private _headers: Headers = new Headers();

    public static newBuilder(): RestRequestBuilder {
        return new RestRequestBuilder();
    }

    public build(): RestRequest {
        return new RestRequest(this);
    }

    public withMethod(method: HTTPMethod): RestRequestBuilder {
        this._method = method;
        return this;
    }

    public withPath(path: string): RestRequestBuilder {
        this._path = path;
        return this;
    }

    public withUrlEncodedQuery(query: object): RestRequestBuilder {
        this._query = querystring.stringify(query as ParsedUrlQueryInput | undefined);
        return this;
    }

    public withJsonBody(body: object): RestRequestBuilder {
        this._body = JSON.stringify(body);
        this._contentType = ContentType.JSON;
        return this;
    }

    public withTextBody(body: string): RestRequestBuilder {
        this._body = body;
        this._contentType = ContentType.TEXT;
        return this;
    }

    public withFormDataBody(body: FormData) {
        this._body = body;
        this._contentType = ContentType.AUTO;
        return this;
    }

    public withHeaders(headers: Headers): RestRequestBuilder {
        this._headers = headers;
        return this;
    }

    get method(): HTTPMethod {
        return this._method;
    }

    get path(): string {
        return this._path;
    }

    get query(): string {
        return this._query;
    }

    get body(): any {
        return this._body;
    }

    get contentType(): ContentType {
        return this._contentType;
    }

    get headers(): Headers {
        return this._headers;
    }

    private constructor() {}
}

export class RestRequest {
    private readonly input: RequestInfo;
    private readonly request: RequestInit;

    public constructor(builder: RestRequestBuilder) {
        const headers: Headers = new Headers(builder.headers);
        if (builder.contentType !== ContentType.AUTO) {
            headers.set("Content-Type", builder.contentType.toString());
        }
        this.input = `${builder.path}`;
        if (builder.query.length > 0) {
            this.input += "?" + builder.query;
        }
        if (builder.method === HTTPMethod.GET) {
            if (builder.body !== null && builder.body !== undefined && builder.body.length > 0) {
                throw new Error("GET Method can't have a body!");
            }
            this.request = { method: builder.method, headers: headers };
        } else {
            this.request = { method: builder.method, headers: headers, body: builder.body };
        }
    }

    public executeAndParseAs<T>(responseType: ContentType): Promise<T> {
        return fetch(this.input, this.request).then((response: Response) => {
            return response.text().then((text) => {
                if (!response.ok) {
                    return Promise.reject(new RestRequestError(response.statusText, response.status, text));
                }
                switch (responseType) {
                    case ContentType.JSON:
                        return text && JSON.parse(text);
                    case ContentType.TEXT:
                        return text;
                    default:
                        return Promise.reject(
                            new RestRequestError("Response type `" + responseType + "` is not implemented!", 415, text)
                        );
                }
            });
        });
    }
}
