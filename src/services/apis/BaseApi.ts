import { ContentType, HTTPMethod, RestRequestBuilder } from "App/services/apis/RestClient";

export abstract class BaseApi {
    private static readonly basePath: string = `${process.env.PUBLIC_URL}/`;

    protected sendRequest<T>(method: HTTPMethod, path: string, data?: any) {
        return RestRequestBuilder.newBuilder()
            .withMethod(method)
            .withPath(BaseApi.basePath + path)
            .withJsonBody(data)
            .build()
            .executeAndParseAs<T>(ContentType.JSON);
    }

    protected requestXmlFile(path: string) {
        return RestRequestBuilder.newBuilder()
            .withMethod(HTTPMethod.GET)
            .withPath(BaseApi.basePath + path)
            .build()
            .executeAndParseAs<string>(ContentType.TEXT);
    }

    protected sendUrlEncodedRequest<T>(
        method: HTTPMethod,
        path: string,
        urlEncodedQuery: object,
        data?: any
    ): Promise<T> {
        return RestRequestBuilder.newBuilder()
            .withMethod(method)
            .withPath(BaseApi.basePath + path)
            .withUrlEncodedQuery(urlEncodedQuery)
            .withJsonBody(data)
            .build()
            .executeAndParseAs<T>(ContentType.JSON);
    }

    protected sendFormData<T>(path: string, formData: FormData): Promise<T> {
        return RestRequestBuilder.newBuilder()
            .withMethod(HTTPMethod.POST)
            .withPath(BaseApi.basePath + path)
            .withFormDataBody(formData)
            .build()
            .executeAndParseAs<T>(ContentType.JSON);
    }
}
