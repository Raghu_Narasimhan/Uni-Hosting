const fs = require("fs");
const fastXmlParser = require("fast-xml-parser");

const publicPath = __dirname + "/../public-assets/";
const integrationsPath = "xml/integrations/";
const integrationsAbsolutePath = publicPath + integrationsPath;

const dependencies = {
    "Method Download": ["Method Start", "Method Selection"],
    "Method Upload": ["Method Start", "Method Selection"],
    "Method Selection": ["Method Start"],
    "Default Method": ["Method Start"],
    "Method Discovery": ["Method Start", "Method Selection"],
    "Method Input Setting": ["Method Start"],
    "Method Output Retrieval": ["Method Start"],
    "Method Progress Retrieval": ["Method Start"],
    "Method Pausing And Resuming": ["Method Start"],
    "Method Cancellation": ["Method Start"],
};

function isSupported(functionality) {
    return !functionality["Availability"] || functionality["Availability"].includes("supported");
}

function validate(integration) {
    const documentedTypes = [];
    const availableTypes = [];
    integration["Functionality"].forEach((functionality) => {
        functionality["Type"].forEach((type) => {
            documentedTypes.push(type);
            if (isSupported(functionality)) {
                availableTypes.push(type);
            }
        });
    });
    availableTypes.forEach((availableType) => {
        if (dependencies[availableType]) {
            dependencies[availableType].forEach((requiredType) => {
                if (!documentedTypes.includes(requiredType)) {
                    throw Error("Functionality '" + availableType + "' requires '" + requiredType + "' to be documented as well.");
                }
            });
        }
    });
}

fs.readdirSync(integrationsAbsolutePath).forEach(function(integrationFile) {
    console.log("Validating functionality dependencies: " + integrationFile);
    const integrationXml = fs.readFileSync(integrationsAbsolutePath + integrationFile).toString();
    const integration = fastXmlParser.parse(integrationXml, { arrayMode: "strict" });
    integration["Integration"].forEach(validate);
});
