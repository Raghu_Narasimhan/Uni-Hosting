<Integration
        xmlns="http://www.universal-integration-layer.org"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://www.universal-integration-layer.org https://unitelabs.gitlab.io/shared/universal_integration_layer/xml/uil_integration.xsd">
    <Name>Freedom EVOware</Name>
    <Vendor>Tecan</Vendor>
    <SupportedInstrument>
        <Vendor>Tecan</Vendor>
        <Model>Freedom EVO</Model>
    </SupportedInstrument>
    <Description>
        Client software to interface with EVOware instruments. Which offers the capability to write scripts in the software that result in methods that can load data from files.

        Additionally, the software provides an API to interface with the software and control methods and interact with methods at runtime.

        The API library is a C# COM API with the following DLL's available to interface:
            - ...
            - ...
            - ...
    </Description>
    <MinimumRequirement>
        - Freedom EVOware Standard 2.1
        - Windows 7
    </MinimumRequirement>
    <Functionality>
        <Type>Method Start</Type>
        <Type>Method Selection</Type>
        <Availability>supported</Availability>
        <Interface>
            <DLL>
                <Implementation>
                    ```csharp
                    evoApi.PrepareProcess(programName);
                    evoProcessId = evoApi.StartProcess(programName, "");
                    ```
                </Implementation>
            </DLL>
        </Interface>
    </Functionality>
    <Functionality>
        <Type>Method Input Setting</Type>
        <Availability>supported</Availability>
        <Interface>
            <DLL>
                <Implementation>
                    Variables to the Tecan script can be set on startup via the API

                    ```csharp
                    void EVOAPILib.ISystem.SetScriptVariable([In] int ScriptID, [MarshalAs(UnmanagedType.BStr), In] string
                    VariableName, [MarshalAs(UnmanagedType.Struct), In] object Value);
                    ```
                </Implementation>
            </DLL>
        </Interface>
    </Functionality>
    <Functionality>
        <Type>Method Input Setting</Type>
        <Availability>supported</Availability>
        <Interface>
            <CSV>
                <Implementation>
                    Within protocols a path to a file can be specified where the input data can be read from.

                    There can be 1-to-N input files to load from. Usually in CSV format.
                </Implementation>
            </CSV>
        </Interface>
    </Functionality>
    <Functionality>
        <Type>Method Output Retrieval</Type>
        <Availability>supported</Availability>
        <Interface>
            <DLL>
                <Implementation>
                    Output data from the EVOware MAY be stored by the EVOware as variables in the currently running protocol which
                    can be retrieved via an API call.

                    ```csharp
                    Object EVOAPILib.ISystem.GetScriptVariable([In] int ScriptID, [MarshalAs(UnmanagedType.BStr), In] string
                    VariableName);
                    ```
                </Implementation>
            </DLL>
        </Interface>
        <Interface>
            <CSV>
                <Implementation>
                    Within protocol a path to the file can be specified where output data will be written to.

                    There can be 1-to-N output files generated. Usually in CSV format
                </Implementation>
            </CSV>
        </Interface>
        <Interface>
            <UNKNOWN>
                <Implementation>
                    Within protocol a path to the file can be specified where output data will be written to.

                    There can be 1-to-N output files generated with an unknown format.
                </Implementation>
            </UNKNOWN>
        </Interface>
    </Functionality>
    <Functionality>
        <Type>Method Progress Retrieval</Type>
        <Availability>supported</Availability>
        <Interface>
            <DLL>
                <Implementation>
                    The progress of the system can be obtained by polling the status method to get the current state of the
                    system. E.g. Running, Loading, Processing, Simulation, etc..

                    The system may be in multiple possible states at any time.

                    ```csharp
                    EVOAPILib.ISystem.GetStatus()
                    ```
                </Implementation>
            </DLL>
        </Interface>
    </Functionality>
    <Functionality>
        <Type>Method Pausing And Resuming</Type>
        <Availability>supported</Availability>
        <Interface>
            <DLL>
                <Implementation>
                    ```csharp
                    void EVOAPILib.ISystem.Pause();
                    ```
                    or
                    ```csharp
                    void EVOAPILib.ISystem.Resume();
                    ```
                </Implementation>
            </DLL>
        </Interface>
    </Functionality>
    <Functionality>
        <Type>Method Cancellation</Type>
        <Availability>supported</Availability>
        <Interface>
            <DLL>
                <Implementation>
                    ```csharp
                    void EVOAPILib.ISystem.Stop()
                    ```
                    or
                    ```csharp
                    void EVOAPILib.ISystem.CancelProcess(int ProcessID)
                    ```
                    NOTE: It is unknown what the difference is between the two
                </Implementation>
            </DLL>
        </Interface>
    </Functionality>
    <Functionality>
        <Type>Device Simulation Setting</Type>
        <Availability>supported</Availability>
        <Interface>
            <DLL>
                <Implementation>
                    The client software can run with a simulated instrument. There must be a simulated hardware running on the
                    client PC.

                    TODO: Steps to start simulated hardware
                    TODO: How to verify simulation
                    TODO: How to start EVO software in simulation

                    ```csharp
                    void EVOAPILib.ISystem.Logon([MarshalAs(UnmanagedType.BStr), In] string UserName,
                    [MarshalAs(UnmanagedType.BStr), In] string Password, [In] int Plus, [In] int Simulation);
                    ```
                </Implementation>
            </DLL>
        </Interface>
    </Functionality>
    <Functionality>
        <Type>Device Initialization</Type>
        <Availability>supported</Availability>
        <Interface>
            <DLL>
                <Implementation>
                    ```csharp
                    var evoApi = new SystemClass();
                    evoApi.SetRemoteMode((Int32) RemoteMode.On);
                    evoApi.Initialize();
                    ```
                </Implementation>
            </DLL>
        </Interface>
    </Functionality>
    <Functionality>
        <Type>Device Termination</Type>
        <Availability>supported</Availability>
        <Interface>
            <DLL>
                <Implementation>
                    When finishing up with using the instrument remote mode must be switched off and must be logged off

                    ```csharp
                    evoApi.SetRemoteMode((Int32) 0);
                    evoApi.Logoff();
                    ```
                </Implementation>
            </DLL>
        </Interface>
    </Functionality>
    <Functionality>
        <Type>Method Removal</Type>
        <Availability>not applicable</Availability>
    </Functionality>
    <Functionality>
        <Type>Method Error Resolution</Type>
        <Availability>not applicable</Availability>
    </Functionality>
    <Functionality>
        <Type>Method Error Retrieval</Type>
        <Availability>supported</Availability>
        <Interface>
            <DLL>
                <Implementation>
                    Errors are thrown within function calls in the form of `Exception`'s but additional errors.

                    One can also register for errors via an event handler like so:

                    ```csharp
                    private static void EvoApiOnErrorEvent(DateTime starttime, DateTime endtime, string device, string macro, string o,
                    string message, short status, string processname, int processid, string macroid)
                    {
                        Log.Warn($"starttime: {starttime}" +
                        $"\nendtime: {endtime}" +
                        $"\ndevice: {device}" +
                        $"\nmacro: {macro}" +
                        $"\no: {o}" +
                        $"\nmessage: {message}" +
                        $"\nstatus: {status}" +
                        $"\nprocessname: {processname}" +
                        $"\nprocessid: {processid}" +
                        $"\nmacroid: {macroid}");
                    }
                    voApi.ErrorEvent += EvoApiOnErrorEvent;
                    ```
                </Implementation>
            </DLL>
        </Interface>
    </Functionality>
</Integration>
