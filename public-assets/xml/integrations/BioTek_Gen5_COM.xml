<Integration
        xmlns="http://www.universal-integration-layer.org"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://www.universal-integration-layer.org https://unitelabs.gitlab.io/shared/universal_integration_layer/xml/uil_integration.xsd">
    <Name>Gen5 COM</Name>
    <Vendor>BioTek</Vendor>
    <SupportedInstrument>
        <Vendor>BioTek</Vendor>
        <Model>Synergy HTX</Model>
    </SupportedInstrument>
    <SupportedInstrument>
        <Vendor>BioTek</Vendor>
        <Model>Synergy Neo2</Model>
    </SupportedInstrument>
    <SupportedInstrument>
        <Vendor>BioTek</Vendor>
        <Model>Epoch 2</Model>
    </SupportedInstrument>
    <SupportedInstrument>
        <Vendor>BioTek</Vendor>
        <Model>ELx808</Model>
    </SupportedInstrument>
    <SupportedInstrument>
        <Vendor>BioTek</Vendor>
        <Model>Cytation</Model>
    </SupportedInstrument>
    <SupportedInstrument>
        <Vendor>BioTek</Vendor>
        <Model>800 TS</Model>
    </SupportedInstrument>
    <Description>
            Gen5’s OLE Automation interface is designed for customers needing to integrate Gen5 and BioTek readers into an automated system.
            Gen5 can function as an OLE Automation server to provide an OLE Automation client application access to most of its features.

            The client application can, for example, use Gen5 to:
            - Create a new experiment
            - Perform a read
            - Export results
            - Print a report

            Gen5 also provides basic reader commands, such as 'open door', 'close door', etc...
            Gen5 can be controlled from any programming language that supports OLE Automation including C++, Visual Basic, C#, applications
            that include VBA (Visual Basic for Applications) such as Excel, and even scripting languages such as VBScript and JScript.
        </Description>
    <MinimumRequirement>
        - BioTek Gen 5
        - Windows 7
    </MinimumRequirement>
    <Functionality>
        <Type>Device Connection</Type>
        <Availability>supported</Availability>
        <Interface>
            <COM>
                <Implementation>
                    Interface has to be configured to connect to a specific instrument via 3 possible physical connections:
                    - Serial
                    - USB
                    - OLE

                    Each interfce requires calling either of the following API's respectively:

                    ```csharp
                    ConfigureSerialReader
                    ```

                    or

                    ```csharp
                    ConfigureUSBReader
                    ```
                    or
                    ```csharp
                    ConfigureOleReader
                    ```

                    See section 2.1.2.1, 2.1.2.2, 2.1.2.3 and 2.1.2.4 of the Manual
                </Implementation>
            </COM>
        </Interface>
    </Functionality>
    <Functionality>
        <Type>Method Selection</Type>
        <Availability>supported</Availability>
        <Interface>
            <COM>
                <Implementation>
                    ##### Application Interface

                    The Application Interface allows automation programmers to select an experiment by calling either the `NewExperiment` or `NewExperimentEx` methods.

                    ###### NewExperiment

                    The `NewExperiment` method will create a new experiment based on the specified protocol.
                    ```
                    Experiment NewExperiment(string ProtocolPathName)
                    ```

                    ###### NewExperimentEx

                    _This method is limited to readers with a type equal to or greater than Synergy2 (Reader Type 10)._

                    The `NewExperimentEx` method is an extension of the `NewExperiment` method. It allows automation programmers to create an experiment
                    based on the specified protocol or protocol metadata using the BTIProtocol XML format.
                    ```
                    Experiment NewExperimentEx(enum enumProtocolDefinitionType, string strProtocolDefinition)
                    ```
                    Refer to Gen5 OLE Automation guide sections 2.1.2.9 and 2.1.2.10.
                </Implementation>
            </COM>
        </Interface>
    </Functionality>
    <Functionality>
        <Type>Method Creation</Type>
        <Availability>not supported</Availability>
    </Functionality>
    <Functionality>
        <Type>Method Removal</Type>
        <Availability>not supported</Availability>
    </Functionality>
    <Functionality>
        <Type>Method Start</Type>
        <Availability>supported</Availability>
        <Interface>
            <COM>
                <Implementation>
                    ##### Plate interface

                    The Plate Interface allows automation programmers to start a read by calling the `StartRead` or `StartReadEx` methods.

                    ###### StartRead
                    Starts a read using the currently configured reader and the read steps defined in the current protocol.
                    ```
                    PlateReadMonitor StartRead()
                    ```

                    ###### StartReadEx
                    The StartReadEx method is similar to the StartRead method but it allows a 180 degrees rotation of the plate.
                    ```
                    PlateReadMonitor StartReadEx(boolean bRotate180Deg)
                    ```
                    Refer to Gen5 OLE Automation guide section 2.4.2.2.
                </Implementation>
            </COM>
        </Interface>
    </Functionality>
    <Functionality>
        <Type>Method Input Setting</Type>
        <Availability>supported</Availability>
        <Interface>
            <COM>
                <Implementation>
                    ##### Experiment and Plate interfaces

                    The Experiment and Plate Interfaces allow the automation programmers to change the settings of an experiment or for a particular plate respectively via various method calls.
                    Refer to Gen5 OLE Automation guide sections 2.2.2 and 2.4.2.
                </Implementation>
            </COM>
        </Interface>
    </Functionality>
    <Functionality>
        <Type>Method Output Retrieval</Type>
        <Availability>supported</Availability>
        <Interface>
            <COM>
                <Implementation>
                    ##### Experiment interface

                    ###### Save

                    The Save method of the Experiment Interface allows the automation programmers to save the experiment to its current location.
                    ```
                    void Save()
                    ```

                    ###### SaveAs

                    The Save method of the Experiment Interface allows the automation programmers to save the experiment to a new location.
                    ```
                    void SaveAs(string FilePath)
                    ```
                    Refer to Gen5 OLE Automation guide sections 2.2.2.2 and 2.2.2.3.
                </Implementation>
            </COM>
        </Interface>
    </Functionality>
    <Functionality>
        <Type>Method Progress Retrieval</Type>
        <Availability>supported</Availability>
        <Interface>
            <COM>
                <Implementation>
                    ##### PlateReadMonitor interface

                    The PlateReadMonitor Interface allows the automation programmers to query Gen5 during a read to determine when the read completes by accessing the `ReadInProgress` property.

                    ###### ReadInProgress

                    True indicates a read is in progress, otherwise false.
                    ```
                    bool PlateReadMonitor.ReadInProgress
                    ```
                    Refer to Gen5 OLE Automation guide section 2.5.1.1.
                </Implementation>
            </COM>
        </Interface>
    </Functionality>
    <Functionality>
        <Type>Method Pausing And Resuming</Type>
        <Availability>supported</Availability>
        <Interface>
            <XML>
                <Implementation>
                    A read can be paused by including a Stop/Resume step in its protocol.
                    Refer to Gen5 OLE Automation guide section 4.5.
                </Implementation>
            </XML>
        </Interface>
        <Interface>
            <COM>
                <Implementation>
                    ##### Plate interface

                    The Plate Interface allows automation programmers to resume a read that has been paused by calling `ResumeRead` method.
                    In the case the read was simulated, automation programmer should use its counterpart, `ResumeSimulatedRead`.
                    Refer to Gen5 OLE Automation guide section 2.4.2.5.
                </Implementation>
            </COM>
        </Interface>
    </Functionality>
    <Functionality>
        <Type>Method Cancellation</Type>
        <Availability>supported</Availability>
        <Interface>
            <COM>
                <Implementation>
                    ##### Plate interface

                    The `AbortRead` method of the Plate Interface allows automation programmers to terminate the current read.
                    ```
                    void AbortRead()
                    ```
                    Refer to Gen5 OLE Automation guide section 2.4.2.8.
                </Implementation>
            </COM>
        </Interface>
    </Functionality>
    <Functionality>
        <Type>Device Simulation Setting</Type>
        <Availability>supported</Availability>
        <Interface>
            <COM>
                <Implementation>
                    ##### Plate interface

                    The `StartSimulatedRead` method of the Plate Interface allows automation programmers to start a simulated read.
                    ```
                    void StartSimulatedRead()
                    ```
                    Refer to Gen5 OLE Automation guide section 2.4.2.3.
                </Implementation>
            </COM>
        </Interface>
    </Functionality>
    <Functionality>
        <Type>Method Upload</Type>
        <Availability>not supported</Availability>
    </Functionality>
    <Functionality>
        <Type>Method Download</Type>
        <Availability>not supported</Availability>
    </Functionality>
    <Functionality>
        <Type>Default Method</Type>
        <Availability>not supported</Availability>
    </Functionality>
    <Functionality>
        <Type>Method Error Resolution</Type>
        <Availability>not supported</Availability>
    </Functionality>
    <Functionality>
        <Type>Method Error Retrieval</Type>
        <Availability>supported</Availability>
        <Interface>
            <COM>
                <Implementation>
                    The `PlateReadMonitor` instance returned when starting the method can be used to retrieve:

                    The number of errors logged can be found in the `PlateReaderMonitor.ErrorCodes` property.


                    Then for each error index the corresponding error message can be retrieved:

                    ```csharp
                    int errorIndex = 0;
                    PlateReadMonitor.GetErrorCode(errorIndex);

                    ```


                    - wether the reader has any errors
                    - the list of available errors

                </Implementation>
            </COM>
        </Interface>
    </Functionality>
    <Functionality>
        <Type>Device Termination</Type>
        <Availability>not applicable</Availability>
    </Functionality>
</Integration>