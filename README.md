# universal_integration_layer

Project to bring transparency and structured documentation on how to interface with instruments. This is initially focused on instruments in the lab and life sciences industry.

## Web Page

The page can be accessed here: https://unitelabs.gitlab.io/shared/universal_integration_layer/

## The rulebook and the process
The basis of this website stems from the [rulebook.md](src/websiteContent/rulebook.md). This defines the vocabulary which is shaped to represent the definition of instruments and their corresponding integration capabilities. Feel free to issue an MR and review.

Out of the rulebook we can then define a schema to represent how instrument integrations should be documented.

From this XML schema we are then able to document instruments and integrations on an XML. For a list of examples such as these merge requests:
  - [Biotek Liquid Handlers](https://gitlab.com/unitelabs/shared/universal_integration_layer/-/merge_requests/18/diffs)
  
  - [Biotek Gen5](https://gitlab.com/unitelabs/shared/universal_integration_layer/-/merge_requests/6/diffs)

  - [Biobanks](https://gitlab.com/unitelabs/shared/universal_integration_layer/-/merge_requests/5)

Feel free to issue a merge request with your desired contributions.

## Prerequisites
- Node.js 10.x 

## Build

```bash
./gradlew build
```

## Run
```bash
yarn start
```